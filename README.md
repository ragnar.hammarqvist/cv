# Ragnar Hammarqvist
1982-12-24

https://gitlab.com/ragnar.hammarqvist/cv
[PDF](https://gitlab.com/ragnar.hammarqvist/cv/-/jobs/artifacts/main/file/CV.pdf?job=build)

## Area of interests
System architecture, Software development, Computer graphics(2D &3D),
modeling and simulation, distributed systems, information visualization,
code.


## Key Skills and Competencies
* Analytic, Organized and Goal oriented
* Strong Knowledge of C/C++98/C++11/C++20
* Knowledge of Dotnet
* Knowledge of Python,C#,Javascript,SQL
* Experience of Serious game development, Unity3D
* Experience of Manned simulators, Real Time systems, Safety critical systems
* Experience of Multi-threading application development.
* Knowledge about modeling and simulation
* Excellent cross platform programming skills on Microsoft Windows and Linux platforms.
* Experience of LVC(Live Virtual Constructive) federation development.
* Experience of working in production critical environment.
* Continuous integration maintenance, implementation and development.
* Experience of agile development: Scrum and Kanban.
* Experience of multi organisation collaboration.
* Experience of project management.
* Expert in simulator interoperability: HLA and DIS.
* Knowledge about web development: Python flask, php, mysql, html5, .NET core, blazor
* Experience of rapid GUI development: QT, GLStudio, Paper prototypes, html5, imgui
* Experience of test driven development:

## Qualities
Sense of responsibility, careful and eager to learn

## Achievements
Overcame Dyslexia

\newpage

## Work Experience

| When          | Where                                                     |  What                                                                                                      |
| :-------------|:----------------------------------------------------------| :---------------------------------------------------------------------------------------------------------:|
| 2014-Present  | FOI (Swedish Defence Research Agency) Kista, Sweden       | Department FLSC(Air force Simulation Center) System Development, System Architect, Project Manager         |
| 2014- 2018    | Right Hand System - Own Individual company                | Part time contracting as developer, 3d animation assignments                                               |
| 2013-2013     | Elekta AB Stockholm, Sweden.                              | R&D Software Department Software Engineer                                                                  |
| 2011-2013     | FOI (Swedish Defence Research Agency) Kista, Sweden       | Department FLSC(Air force Simulation Center) Research Engineer, System Development                         |
| 2008 –2011    | FOI (Swedish Defence Research Agency) Linköping, Sweden   | R&D Software Department Software Engineer                                                                  |
| 2008 –2008    | FOI (Swedish Defence Research Agency) Linköping, Sweden   | Research Engineer, System Development Consulting                                                           |
| 2006 –2007    | Clas Ohlson Central Warehouse Insjön, Sweden              | Warehouse Personnel Part-time employee during summer and winter vacations                                  |

## Education

| When          | Where                                                                       | What                                            |
| :-------------|:----------------------------------------------------------------------------| :-----------------------------------------------|
| 2002– 2008    | Linköping Institute of Technology, Linköping University Norrköping, Sweden  | MSc. in Media Technologies and Engineerin       |
| 1999 – 2002   | Soltorgs Gymnasium Borlänge, Sweden.                                        | Upper Secondary School (Am. Senior High School) |

## Publications
Improving Tactical Data Link Simulation Standards To Better Support LVC Exercises
Conference: 2018 Winter Simulation Innovation Workshop (SIW), Florida, USA


## Conference attended

2018 CppCon, Washington - USA

2018 Winter Simulation Conference, Gothenburg

2016 I/ITSEC, Orlando - USA


## Courses attended
2018-06 Project Management, Laerning Tree, Stockholm

2018-03 Git for developer, Informator, Stockholm

2017-01 GlStudio, Orlando USA

2015-09 Test Driven Development, Learning Tree, Stockholm

2013-01 Flight and Ground Vehicle Simulation Binghamton University State University of New York 

2010-05 Effective Concurrency PC-Ware Herb Sutter, Kista 

2009-11 Design Patterns, ENEA Mårten Berggren, Lund 

2009-11 HLA 1516 Hands-On Pitch Technologies Björn Mölen Linköping

## Languages 
* Swedish mother tongue
* English speaking, reading and writing fluently
